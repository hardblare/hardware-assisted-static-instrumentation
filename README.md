# Instrumentation using CoreSight PTM component and Kernel modification

The work made available here is described in the following paper: 

Muhammad Abdul Wahab, Pascal Cotret, Mounit Nasr Allah, Guillaume Hiet, Vianney Lapôtre, Guy Gogniat and Arnab Kumar Biswas.
**A novel lightweight hardware-assisted static instrumentation approach for ARM SoC using debug components.**
*In Asian Hardware Oriented Security and Trust Symposium (AsianHOST), pages 1–6, Dec 2018*

Preprint: https://bitbucket.org/hardblare/hw-static-instrumentation/src/master/paper/paper-preprint.pdf (uploaded on arXiv asap) 

---
**Key idea:** Add system call that writes a value to the CONTEXTIDR register which is written into trace by PTM component

:heavy_check_mark: Allows to recover information from both userland and kernel land with the same interface. If we compare our instrumentation solution to other DBT (Dynamic Binary Translation) tools such as dynamoRIO, we will be better in terms of runtime overhead even though it uses a syscall. In addition, the development time for using dynamoRIO and what we propose is much interesting. Requires adding a single line of code! 

:x: Adds important runtime overhead as a syscall is required each time the instrumentation is required. 

---
[TOC]

# Requirements
- SD card with two partitions 
    - FAT32 for boot 
    - ext4 for rootfs (file system)

## Instructions for SD card 
To partition SD card, guidelines can be followed from [this link](https://www.emcraft.com/imxrt1050-evk-board/sd-card-partitioning).

The files that need to be copied into boot folder are available in `boot` folder. These copies should be copied into the FAT32 partition which is the first partition.

The files that need to be copied into boot folder are available in `rootfs.tar.gz` file. The `rootfs.tar.gz` file can be unzipped into second partition using the following command: `sudo tar xvzf rootfs.tar.gz -C /dev/sdc2` where /dev/sdc2 is the mounted location of the SD card. 

**PLEASE BE VERY VERY CAREFUL ABOUT THIS LOCATION BECAUSE YOU CAN OVERWRITE YOUR HARD DRIVE CONTENT**

# Detailed instructions (skippable)
The instructions available in this section are already included in the kernel files provided in the boot folder. Therefore, it can be safely skipped. The description here is made available so that a user who wants to use the original version of linux-xlnx kernel and start from there to produce the images made available in boot folder. 

## Add system call (syscall) in Linux
To add a system call, we need to modify three files. This is true for the Linux kernel v4.7 

1. Add function definition in `kernel-source/include/linux/syscalls.h`
2. Add function code in `kernel-source/kernel/sys.c`
3. Add an entry for system call in `kernel-source/arch/arm/kernel/calls.S` 

The changes made to the kernel are: 

```diff
--- linux.vanilla/arch/arm/kernel/calls.S   2017-09-15 15:53:57.483431664 +0200
+++ linux-xlnx-v2017.1-hardblare/arch/arm/kernel/calls.S    2017-11-10 10:25:53.492663713 +0100
@@ -406,6 +406,8 @@
        CALL(sys_pkey_mprotect)
 /* 395 */  CALL(sys_pkey_alloc)
        CALL(sys_pkey_free)
+       CALL(sys_wctxtid)
+       CALL(sys_enable_pmu)
--- linux.vanilla/include/linux/syscalls.h  2017-09-15 15:54:15.443497268 +0200
+++ linux-xlnx-v2017.1-hardblare/include/linux/syscalls.h   2017-11v10 10:28:22.302881935 +0100
@@ -221,6 +221,9 @@
 asmlinkage long sys_nanosleep(struct timespec __user *rqtp, struct timespec __user *rmtp);
 asmlinkage long sys_alarm(unsigned int seconds);
 asmlinkage long sys_getpid(void);
+//asmlinkage long sys_wctxtid(void);
+asmlinkage long sys_wctxtid(unsigned int instrumentation_data);
+asmlinkage long sys_enable_pmu(void);
 asmlinkage long sys_getppid(void);
 asmlinkage long sys_getuid(void);
 asmlinkage long sys_geteuid(void);
--- linux.vanilla/kernel/sys.c  2017-09-15 15:54:16.203500043 +0200
+++ linux-xlnx-v2017.1-hardblare/kernel/sys.c   2017-11-10 10:55:56.649769441 +0100
@@ -834,10 +834,38 @@
 
+SYSCALL_DEFINE1(wctxtid, unsigned int *, instrumentation_data)
+{
+   unsigned int buf[64];
+   asm volatile(
+       "   mcr p15, 0, %0, c13, c0, 1\n"
+//     ::"r"(task_pid_vnr(current)):);
+       ::"r"(instrumentation_data):);  
+   isb();
+   return 0;   
+}
+
```

Then we compile the kernel using one of the following two commands 

```bash
bitbake linux-xlnx  
bitbake linux-xlnx -c compile -f && bitbake linux-xlnx -c deploy
```

Once the kernel is booted on the device, make sure that the system call has been added by listing all syscalls available.

```bash
cat /proc/kallsysms | grep "added_syscall_name"
cat /proc/kallsysms | grep "sys_wctxtid"

root@zedboard-hardblare:~/tests_appli# cat /proc/kallsyms | grep wctxtid
c0035aa4 T sys_wctxtid
```
# Hardware design (Skippable)

The design used in order to decode trace and store it into BRAM is shown in the following Figure. The bitstream used in the next sections is generated using this design. The project files are available in `hw/hw_project.zip` file.

![hw_design](hw_design.png)


# How to use the syscall in C source code? 

We can use the syscall in C source code as in the following example code.

```c
#include <stdio.h>
#include <linux/kernel.h>
#include <sys/syscall.h>
#include <unistd.h>
int main(){
    unsigned data_to_send = 0xffffffff;
    // system call number is 397 (cf calls.S file change)
    long int amma = syscall(397, data_to_send);
    // printf is just to make sure that the syscall works properly 
    // The syscall always return zero
    printf("System call sys_wctxtid returned %ld\n", amma);
    return 0;
}
```

Then we compile and run the program 

```bash
gcc test_syscall.c -o test_syscall
./test_syscall

root@zedboard-hardblare:~/tests_appli# ./test_syscall
System call sys_wctxtid returned 0
```

Change kernel printk log level => default is 4.

```bash
echo 0 > /proc/sys/kernel/printk
```

`test_sycall.c` (main function range) => 
It can change depending on the compiler used for compiling the application. Here the addresses are the following. These addresses can be obtained by using objdump on the binary file. 
```bash
10478 # start address 
104e0 # end address 
```

# Trace recovery

Now that the SD card is setup and the C code changes are explained, we can have a look at the generated trace. 

## Raw trace from ETB (on-chip RAM)
The trace on ETB can be obtained using the `trace_etb` script. The script takes care of configuring CoreSight components and to run the application. Then, the trace is printed to the terminal. The shown trace in the following script is different from the one you will obtain. The additional instructions added in the traced program by a user may not be the same as the one we added. If use_case.elf is traced, then the additional trace should be: `cd ab 34 12`.

```bash
root@zedboard-hardblare:~/tests_appli# ./trace_etb
Hi
Hello0xc100
8+0 records in
8+0 records out
4096 bytes (4.1 kB, 4.0 KiB) copied, 0.0194763 s, 210 kB/s
00000000  00 00 00 00 00 80 08 78  04 01 00 21 f4 ee 03 00  |.......x...!....|
00000010  8b 03 08 8c 04 01 00 21  f4 ee 03 00 9d 03 08 98  |.......!........|
00000020  04 01 00 21 ff ff ff ff  9d 03 08 a8 04 01 00 21  |...!...........!|
00000030  dd dd dd dd 85 03 08 b4  04 01 00 21 dd dd dd dd  |...........!....|
00000040  9d 03 08 c4 04 01 00 21  aa aa aa aa 9d 03 08 d4  |.......!........|
00000050  04 01 00 21 11 11 11 11  fd bc cf db 0d 01 00 00  |...!............|
00000060  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
```

Here, the trace contains instrumented data such as 0xffff ffff and 0xdddd dddd and so on. 

## Decoded trace from PFT decoder via TPIU and EMIO interface
Now the TPIU trace sink will be used to recover traces on the FPGA. Then, the bitstream contains a design in which a PFT decoder takes care of decoding raw traces and storing them in a BRAM used in true dual port configuration. The instrumented data shown in the next listing is reading back the BRAM content and shows the instrumented data. 

```bash
root@zedboard-hardblare:~/tests_appli# ./trace_tpiu
ffffffff dddddddd aaaaaaaa 11111111
```

# Use case - Double Free detection

The instrumentation strategy can be used in order to detect double free attacks. 

```C
// Code from https://github.com/n30m1nd/Linux_Heap_Exploitation_Intro_Series/blob/master/FREE_UNVALIDATED/double-free/topleaks.c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define WORD_SIZE ((size_t)-1 > 0xffffffffUL ? 8 : 4)
#define SIZEOFNORMALCHUNK 0x100-WORD_SIZE
#define SIZEOFFASTCHUNK 0x60-WORD_SIZE

int main()
{
    char *A, *B, *C;  // line 1
    A = malloc(SIZEOFNORMALCHUNK); // line 2
    C = malloc(SIZEOFNORMALCHUNK); // line 3
    free(A); // line 4
    B = malloc(SIZEOFNORMALCHUNK); // same location as A // line 5
    free(A); // line 6
    return 0; // line 7
}
```

The double free attack happens here because after freeing A at the line 6 as the allocated area A has been already freed at the line 4. 

The simple strategy in order to detect such attacks using the proposed instrumentation strategy is to add the lines shown in the following listing.

```C
A = malloc(SIZEOFNORMALCHUNK);
syscall(397,0xfff00001); // instrumented code
C = malloc(SIZEOFNORMALCHUNK);
syscall(397,0xfff00002); // instrumented code
free(A);
syscall(397,0xffe00001); // instrumented code
B = malloc(SIZEOFNORMALCHUNK); // same location as A
free(A); // instrumented code
syscall(397,0xffe00001);
```

Each time `malloc` (or similar functions such as `calloc`) is called, the function writes the allocated address value using our proposed syscall. Each time `free` (or similar function) is called, the address to free is sent to the FPGA using our proposed syscall. Using the decoded trace on the FPGA, the proposed system can detect whether the address received has been allocated or freed. This way, we can keep track, on the FPGA, of allocated and freed memory addresses.

Output
``` bash
root@zedboard-hardblare:~/tests_appli# ./trace_tpiu_topleaks
TURN OFF CPU1
[ 8497.608896] coresight-tpiu f8803000.tpiu: TPIU enabled
[ 8497.613995] coresight-replicator amba:replicator: REPLICATOR enabled
[ 8497.620297] coresight-funnel f8804000.funnel: FUNNEL inport 0 enabled
[ 8497.626790] coresight-etm3x f889c000.ptm0: ETM tracing enabled

[+] malloc A(0xfc)

[+] malloc B(0xfc)
[*] Now pay attention t[ 8497.640728] coresight-etm3x f889c000.ptm0: ETM tracing disabled
o the contents of B
B is still valid in the code but under the [ 8497.648052] coresight-funnel f8804000.funnel: FUNNEL inport 0 disabled
hood
                it is free for the allocator... B contents [Ìçõ¶Ìçõ¶]
C[ 8497.660119] coresight-replicator amba:replicator: REPLICATOR disabled
ontents of B in hex: cce7f5b6cce7f5b6000000000000000000000000000[ 8497.672080] coresight-tpiu f8803000.tpiu: TPIU disabled
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000010000
We leaked main_arena->top through A's FD and BK which can be accessed through B's variable
A->FD = main_arena->top = 0xb6f5e7cc
/dev/mem opened.
Memory mapped at address 0xb6f4a000.
00 106a0 10358 106c0 104d4 106ec b6e3ec88 1057c 10378 10598 1039c 105a0 103c0 105b8 1039c 105c0
103c0 105d8 10384 105e0 103c0 105f0 10378 10600 1039c 10608 103c0 10620 10384 10628 103c0 10638
10390 10644 10378 10654 10378 10660 1050c 1066c 10390 10678 10378 10690 b6e3ecf8 b6e3ec00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00
root@zedboard-hardblare:~/tests_appli# ./appli_trace_context.elf
/dev/mem opened.
Memory mapped at address 0xb6fba000.
00 46084 fff00001 fff00002 ffe00001 fff00003 ffe00001 00 00 00 00 00 00 00 00 00
```

Using decoded trace, it can be seen easily that the first allocated value has been freed twice. This is a simple example in which the detection can be done statically. However, in real-world scenarios, dynamic detection scheme may be required and instrumentation is used in order to recover allocated/freed addresses. Therefore, our proposed solution can be used in order to efficiently detect this type of attacks. 

Other attacks that can be detected using our proposed instrumentation strategy are Use-after-free, buffer overflow... 