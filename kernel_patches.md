This file describes different kernel patches in order to take advantage of CoreSight components used in this work. The patch was generate on Linux kernel v4.7 (linux-xlnx). 

# Linux Kernel patches 

## CoreSight (CS) components 

### Device tree

#### Adding support for CS components in Zynq SoC

```bash
--- linux-4.7/arch/arm/boot/dts/zynq-7000.dtsi.orig 2016-07-24 21:23:50.000000000 +0200
+++ linux-4.7/arch/arm/boot/dts/zynq-7000.dtsi  2016-10-03 15:54:35.228460164 +0200
@@ -96,6 +96,51 @@
            rx-fifo-depth = <0x40>;
        };
 
+       etb@f8801000 {
+           compatible = "arm,coresight-etb10", "arm,primecell";
+           reg = <0xf8801000 0x1000>;
+           coresight-default-sink;
+           clocks = <&clkc 47>;
+           clock-names = "apb_pclk";
+           port {
+               etb_in_port: endpoint@0 {
+                   slave-mode;
+                   remote-endpoint = <&replicator_out_port0>;
+               };
+           };
+       };
+
+       funnel@f8804000 {
+           compatible = "arm,coresight-funnel", "arm,primecell";
+           reg = <0xf8804000 0x1000>;          
+           clocks = <&clkc 47>;
+           clock-names = "apb_pclk";
+           ports {
+               #address-cells = <0x1>;
+               #size-cells = <0x0>;
+               port@0 {
+                   reg = <0x0>;
+                   funnel_out_port0: endpoint {
+                       remote-endpoint = <&replicator_in_port0>;
+                   };
+               };
+               port@1 {
+                   reg = <0x0>;
+                   funnel_in_port0: endpoint {
+                       slave-mode;
+                       remote-endpoint = <&ptm0_out_port>;
+                   };
+               };
+               port@2 {
+                   reg = <0x1>;
+                   funnel_in_port1: endpoint {
+                       slave-mode;
+                       remote-endpoint = <&ptm1_out_port>;
+                   };
+               };
+           };
+       };
+
        gpio0: gpio@e000a000 {
            compatible = "xlnx,zynq-gpio-1.0";
            #gpio-cells = <2>;
@@ -311,6 +356,59 @@
            clocks = <&clkc 4>;
        };
 
+       ptm0@f889c000 {
+           compatible = "arm,coresight-etm3x", "arm,primecell";
+           reg = <0xf889c000 0x1000>;
+           cpu = <&cpu0>;
+           clocks = <&clkc 47>;
+           clock-names = "apb_pclk";
+           port {
+               ptm0_out_port: endpoint {
+                   remote-endpoint = <&funnel_in_port0>;
+               };
+           };
+       };
+
+       ptm1@f889d000 {
+           compatible = "arm,coresight-etm3x", "arm,primecell";
+           reg = <0xf889d000 0x1000>;
+           cpu = <&cpu1>;
+           clocks = <&clkc 47>;
+           clock-names = "apb_pclk";
+           port {
+               ptm1_out_port: endpoint {
+                   remote-endpoint = <&funnel_in_port1>;                   
+               };
+           };
+       };
+
+       replicator {
+           compatible = "arm,coresight-replicator";
+           ports {
+               #address-cells = <0x1>;
+               #size-cells = <0x0>;
+               port@0 {
+                   reg = <0x0>;
+                   replicator_out_port0: endpoint {
+                       remote-endpoint = <&etb_in_port>;
+                   };
+               };
+               port@1 {
+                   reg = <0x1>;
+                   replicator_out_port1: endpoint {
+                       remote-endpoint = <&tpiu_in_port>;
+                   };
+               };
+               port@2 {
+                   reg = <0x0>;
+                   replicator_in_port0: endpoint {
+                       slave-mode;
+                       remote-endpoint = <&funnel_out_port0>;
+                   };
+               };
+           };
+       };
+
```

### PTM

Adding support for branch broadcasting feature of CS components in Zynq SoC

```bash
diff -uprN -X linux-4.7-vanilla/Documentation/dontdiff linux-4.7-vanilla/drivers/hwtracing/coresight/coresight-etm3x-sysfs.c linux-4.7/drivers/hwtracing/coresight/coresight-etm3x-sysfs.c
--- linux-4.7-vanilla/drivers/hwtracing/coresight/coresight-etm3x-sysfs.c	2016-07-24 21:23:50.000000000 +0200
+++ linux-4.7/drivers/hwtracing/coresight/coresight-etm3x-sysfs.c	2016-09-28 15:36:39.886542702 +0200
@@ -145,7 +145,7 @@ static ssize_t mode_store(struct device
 			goto err_unlock;
 		}
 		config->ctrl |= ETMCR_STALL_MODE;
-	 } else
+	} else
 		config->ctrl &= ~ETMCR_STALL_MODE;
 
 	if (config->mode & ETM_MODE_TIMESTAMP) {
@@ -163,6 +163,20 @@ static ssize_t mode_store(struct device
 	else
 		config->ctrl &= ~ETMCR_CTXID_SIZE;
 
+	if (config->mode & ETM_MODE_BBROAD)
+		config->ctrl |= ETMCR_BRANCH_BROADCAST;
+	else
+		config->ctrl &= ~ETMCR_BRANCH_BROADCAST;
+
+	if (drvdata->arch == (PFT_ARCH_V1_0 | PFT_ARCH_V1_1)) {
+		if (config->mode & ETM_MODE_RET_STACK) {
+			if (config->mode & ETM_MODE_BBROAD)
+				dev_warn(drvdata->dev, "behavior is unpredictable\n");
+			config->ctrl |= ETMCR_RETURN_STACK_EN;
+		} else
+			config->ctrl &= ~ETMCR_RETURN_STACK_EN;
+	}
+
 	if (config->mode & (ETM_MODE_EXCL_KERN | ETM_MODE_EXCL_USER))
 		etm_config_trace_mode(config);
diff -uprN -X linux-4.7-vanilla/Documentation/dontdiff linux-4.7-vanilla/drivers/hwtracing/coresight/coresight-etm.h linux-4.7/drivers/hwtracing/coresight/coresight-etm.h
--- linux-4.7-vanilla/drivers/hwtracing/coresight/coresight-etm.h	2016-07-24 21:23:50.000000000 +0200
+++ linux-4.7/drivers/hwtracing/coresight/coresight-etm.h	2016-09-28 15:35:59.862544418 +0200
@@ -89,11 +89,13 @@
 /* ETMCR - 0x00 */
 #define ETMCR_PWD_DWN		BIT(0)
 #define ETMCR_STALL_MODE	BIT(7)
+#define ETMCR_BRANCH_BROADCAST	BIT(8)
 #define ETMCR_ETM_PRG		BIT(10)
 #define ETMCR_ETM_EN		BIT(11)
 #define ETMCR_CYC_ACC		BIT(12)
 #define ETMCR_CTXID_SIZE	(BIT(14)|BIT(15))
 #define ETMCR_TIMESTAMP_EN	BIT(28)
+#define ETMCR_RETURN_STACK_EN	BIT(29)  /* PTM v1.0 & PTM v1.1 */
 /* ETMCCR - 0x04 */
 #define ETMCCR_FIFOFULL		BIT(23)
 /* ETMPDCR - 0x310 */
@@ -110,8 +112,11 @@
 #define ETM_MODE_STALL		BIT(2)
 #define ETM_MODE_TIMESTAMP	BIT(3)
 #define ETM_MODE_CTXID		BIT(4)
+#define ETM_MODE_BBROAD	BIT(5)
+#define ETM_MODE_RET_STACK	BIT(6)
 #define ETM_MODE_ALL		(ETM_MODE_EXCLUDE | ETM_MODE_CYCACC | \
 				 ETM_MODE_STALL | ETM_MODE_TIMESTAMP | \
+				 ETM_MODE_BBROAD | ETM_MODE_RET_STACK | \
 				 ETM_MODE_CTXID | ETM_MODE_EXCL_KERN | \
 				 ETM_MODE_EXCL_USER)
```

### TPIU

#### Adding CS TPIU driver in Linux kernel

```bash
diff -ur linux.vanilla/drivers/hwtracing/coresight/coresight-tpiu.c linux-xlnx-v2017.1-hardblare/drivers/hwtracing/coresight/coresight-tpiu.c
--- linux.vanilla/drivers/hwtracing/coresight/coresight-tpiu.c	2017-09-15 14:48:30.000000000 +0200
+++ linux-xlnx-v2017.1-hardblare/drivers/hwtracing/coresight/coresight-tpiu.c	2017-09-15 17:25:10.472703906 +0200
@@ -47,7 +47,14 @@
 
 /** register definition **/
 /* FFCR - 0x304 */
-#define FFCR_FON_MAN		BIT(6)
+#define TPIU_FFCR_EN_FTC	BIT(0)
+#define TPIU_FFCR_FON_MAN	BIT(6)
+#define TPIU_FFCR_STOP_FI	BIT(12)
+#define TPIU_FFCR_STOP_TRIGGER	BIT(13)
+
+#define TRACE_PORT_WIDTH	BIT(8)
+#define TPIU_FFCR_BIT		6
+#define TPIU_FFSR_BIT		1
 
 /**
  * @base:	memory mapped base address for this component.
@@ -60,6 +67,15 @@
 	struct device		*dev;
 	struct clk		*atclk;
 	struct coresight_device	*csdev;
+	u32			tpiu_ffcr;
+	u32			tpiu_ffsr;
+	u32			tpiu_current_port_size;
+	u32			tpiu_supported_trigmodes;
+	u32			tpiu_trig_counterval;
+	u32			tpiu_trig_mult;
+	u32			tpiu_fsync_counter;
+	u32			tpiu_extctl_inport;
+	u32			tpiu_extctl_outport;
 };
 
 static void tpiu_enable_hw(struct tpiu_drvdata *drvdata)
@@ -67,7 +83,10 @@
 	CS_UNLOCK(drvdata->base);
 
 	/* TODO: fill this up */
-
+	/* set trace port width */
+	writel_relaxed(1<<7, drvdata->base + TPIU_CURR_PORTSZ);
+	/* clear formatter control register */
+	writel_relaxed(0, drvdata->base + TPIU_FFCR);
 	CS_LOCK(drvdata->base);
 }
 
@@ -84,11 +103,28 @@
 static void tpiu_disable_hw(struct tpiu_drvdata *drvdata)
 {
 	CS_UNLOCK(drvdata->base);
+	unsigned int ffcr;
+	ffcr = readl_relaxed(drvdata->base + TPIU_FFCR);
+	/* stop formatter when a stop has completed */
+	ffcr |= TPIU_FFCR_STOP_FI;
+	/* Clear formatter control reg. */
+	writel_relaxed(ffcr, drvdata->base + TPIU_FFCR);
+	/* Generate manual flush */	
+	ffcr |= TPIU_FFCR_FON_MAN;
+	writel_relaxed(ffcr, drvdata->base + TPIU_FFCR);
+	
+	if (coresight_timeout(drvdata->base, TPIU_FFCR, TPIU_FFCR_BIT, 0)) {
+			dev_err(drvdata->dev,
+					"timeout observed when probing at offset %#x\n",
+					TPIU_FFCR);
+	}
+	
+	if (coresight_timeout(drvdata->base, TPIU_FFSR, TPIU_FFSR_BIT, 1)) {
+			dev_err(drvdata->dev,
+					"timeout observed when probing at offset %#x\n",
+					TPIU_FFCR);
+	}
 
-	/* Clear formatter controle reg. */
-	writel_relaxed(0x0, drvdata->base + TPIU_FFCR);
-	/* Generate manual flush */
-	writel_relaxed(FFCR_FON_MAN, drvdata->base + TPIU_FFCR);
 
 	CS_LOCK(drvdata->base);
 }
@@ -111,6 +147,51 @@
 	.sink_ops	= &tpiu_sink_ops,
 };
 
+#define coresight_simple_func(name, offset)                             \
+static ssize_t name##_show(struct device *_dev,                         \
+                          struct device_attribute *attr, char *buf)    \
+{                                                                       \
+	struct tpiu_drvdata *drvdata = dev_get_drvdata(_dev->parent);    \
+	return scnprintf(buf, PAGE_SIZE, "0x%x\n",                      \
+		readl_relaxed(drvdata->base + offset));        \
+}                                                                       \
+DEVICE_ATTR_RO(name)
+
+coresight_simple_func(tpiu_ffcr, TPIU_FFCR);
+coresight_simple_func(tpiu_ffsr, TPIU_FFSR);
+coresight_simple_func(tpiu_current_port_size, TPIU_CURR_PORTSZ);
+coresight_simple_func(tpiu_supported_trigmodes, TPIU_SUPP_TRIGMODES);
+coresight_simple_func(tpiu_trig_counterval, TPIU_TRIG_CNTRVAL);
+coresight_simple_func(tpiu_trig_mult, TPIU_TRIG_MULT);
+coresight_simple_func(tpiu_fsync_counter, TPIU_FSYNC_CNTR);
+coresight_simple_func(tpiu_extctl_inport, TPIU_EXTCTL_INPORT);
+coresight_simple_func(tpiu_extctl_outport, TPIU_EXTCTL_OUTPORT);
+
+static struct attribute *coresight_tpiu_mgmt_attrs[] = {
+	&dev_attr_tpiu_ffcr.attr,
+	&dev_attr_tpiu_ffsr.attr,
+	&dev_attr_tpiu_current_port_size.attr,
+	&dev_attr_tpiu_supported_trigmodes.attr,
+	&dev_attr_tpiu_trig_counterval.attr,
+	&dev_attr_tpiu_trig_mult.attr,
+	&dev_attr_tpiu_fsync_counter.attr,
+	&dev_attr_tpiu_extctl_inport.attr,
+	&dev_attr_tpiu_extctl_outport.attr,
+	NULL,
+};
+
+static const struct attribute_group coresight_tpiu_mgmt_group = {
+	.attrs = coresight_tpiu_mgmt_attrs,
+	.name = "mgmt",
+};
+
+static const struct attribute_group *coresight_tpiu_groups[] = {
+	&coresight_tpiu_mgmt_group,
+	NULL,
+};
+
+
+
 static int tpiu_probe(struct amba_device *adev, const struct amba_id *id)
 {
 	int ret;
@@ -150,7 +231,7 @@
 	drvdata->base = base;
 
 	/* Disable tpiu to support older devices */
-	tpiu_disable_hw(drvdata);
+	//tpiu_disable_hw(drvdata);
 
 	pm_runtime_put(&adev->dev);
 
@@ -159,6 +240,7 @@
 	desc.ops = &tpiu_cs_ops;
 	desc.pdata = pdata;
 	desc.dev = dev;
+	desc.groups = coresight_tpiu_groups;
 	drvdata->csdev = coresight_register(&desc);
 	if (IS_ERR(drvdata->csdev))
 		return PTR_ERR(drvdata->csdev);

```

